import {
  divCreator,
  buttonCreator,
  buttonOfDiv,
  formCreator,
  inputCreator
} from './creators.js'
import {
  newUserLogin,
  newUserPassword,
  userEnterName,
  userEnterPass,
  userEnter,
  newUserRegistration,
  loginEsc,
  formToggle,
  gameButtonToggle,
  repeatButtonToggle,
  userInLoginKey,
  loginCheck,
  escGame,
  escButtonToggle


} from './functions.js';

import {
  createCanvas,
  game,
} from './game.js'

//Создаём контейнер
const wrapper = divCreator(document.body, "wrapper");
wrapper.setAttribute("id", "wrapper");
const title = divCreator(wrapper, "title");
title.innerText = "Snake Game"


//Создаём хедер
const header = divCreator(wrapper, "header");
const userInLogin = divCreator(header, "userInLogin");
const exitButton = buttonOfDiv(header, "<a href='#'>Выход</a>", "exitButton");
exitButton.addEventListener('click', loginEsc);





//Создаём форму входа
const enterForm = formCreator(wrapper, "enterForm", "<h1>Вход</h1>", "#", "enterForm");
const enterName = inputCreator(enterForm, "text", "enterName", "Введите логин", "entName");
enterName.addEventListener('change', userEnterName)
const enterPassword = inputCreator(enterForm, "password", "enterPassword", "Введите пароль", "entPass");
enterPassword.addEventListener('change', userEnterPass);
const enterButton = buttonCreator(enterForm, "Войти", "enterButton");
enterButton.addEventListener('click', userEnter);
const toRegButton = buttonCreator(enterForm, "Регистрация", "", "regButton");
toRegButton.addEventListener('click', formToggle);

//Создаём форму регистрации
const regForm = formCreator(wrapper, "regForm", "<h1>Регистрация</h1>", "#", "regForm");
const name = inputCreator(regForm, "text", "name", "Придумайте логин", "regName");
name.addEventListener('change', newUserLogin);
const password = inputCreator(regForm, "password", "password", "Придумайте пароль", "regPass");
const repeatPassword = inputCreator(regForm, "password", "password", "Повторите пароль", "regRepass");
repeatPassword.addEventListener('change', newUserPassword);
const buttons = divCreator(regForm, "confButtons");
const reg = buttonCreator(buttons, "Подтвердить", "confirmButton", "reg");
reg.addEventListener('click', newUserRegistration);
const cancel = buttonCreator(buttons, "Отмена", "canсelButton", "cancel");
cancel.addEventListener("click", formToggle);

//Создаём кнопку запуска игры
const gameButton = buttonOfDiv(wrapper, "<a href='#'>Играть</a>", "gameButton", "gameButton")
gameButton.addEventListener("click", createCanvas);
gameButton.addEventListener("click", game);
gameButton.addEventListener("click", gameButtonToggle);
gameButton.addEventListener("click", escButtonToggle);



//Кнопка повтора
const repeatButton = buttonOfDiv(wrapper, "<a href='#'>Повторить</a>", "repeatButton", "repeatButton")
repeatButton.addEventListener("click", canReload);
repeatButton.addEventListener("click", repeatButtonToggle);
//repeatButton.addEventListener("click", game);

// Кнопка выхода
const escGameButton = buttonOfDiv(wrapper, "<a href='#'>Выйти из игры</a>", "escGameButton", "escGameButton")
escGameButton.addEventListener("click", escGame)
//Кнопка инфо
const info = buttonOfDiv(wrapper, "<a href='#'>info</a>", "footer");

//Добавляем залогиненного пользователя на экран
userInLogin.innerText = ("Здравствуйте " + userInLoginKey() + " !");


//Проверяем есть ли залогиненный пользователь
loginCheck();

// 



function canReload() {
  let can = document.querySelector(".canWrapper");
  can.remove();
  createCanvas();
  game();
}