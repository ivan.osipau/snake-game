//  Функция контейнер для игры

export function createCanvas() {
  // Добавляем холст канваса внутрь контейнера
  const wrapper = document.getElementById("wrapper");
  wrapper.insertAdjacentHTML("afterBegin",
    `<div class="canWrapper" ><canvas id="canvas" width="400" height="400"></canvas></div>`
  );
};

export function game() {
  const gameButton = document.getElementById("gameButton");

  //Определяем параметры канваса
  const ctx = canvas.getContext("2d");
  const width = canvas.width;
  const height = canvas.height;
  // Параметры размеров холста
  const boardCell = 20;
  const boardWidth = width / boardCell;
  const boardHeight = height / boardCell;


  //Создаём игровые элементы

  //Игровое поле
  function border(canName, color) {
    canName.fillStyle = color;
    canName.fillRect(0, 0, width, boardCell);
    canName.fillRect(0, (height - boardCell), width, boardCell);
    canName.fillRect(0, 0, boardCell, height);
    canName.fillRect(width - boardCell, 0, boardCell, height);
  };

  //Счётчик очков
  let score = 0;
  const counter = function () {

    ctx.font = "20px Courier",
      ctx.fillStyle = "Red";
    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText("Счёт: " + score, boardCell, boardCell)

  }
  counter();

  //Остановка игры
  const stopGame = function (score) {
    clearInterval(gameLoop);
    repeatButton.classList.toggle("active");
    ctx.font = "50px Courier",
      ctx.fillStyle = "Red";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("Конец игры", width / 2, height / 2);
  };

  //Создание ячеек
  class Cell {
    constructor(col, row) {
      this.col = col;
      this.row = row;
    }
  };
  Cell.prototype.drawCell = function (color) {
    let x = this.col * boardCell;
    let y = this.row * boardCell;
    ctx.fillStyle = color;
    ctx.fillRect(x, y, boardCell, boardCell);
  }
  //съедание кролика
  Cell.prototype.eat = function (eat) {
    return this.col === eat.col && this.row === eat.row;
  };

  //Тело змеи  
  class Snake {
    constructor() {
      this.elements = [
        new Cell(7, 5),
        new Cell(6, 5),
        new Cell(5, 5),
      ];
      // стартовое направление
      this.way = "right";
      this.nextWay = "right";
    }
  };

  // Отрисовка змеи
  Snake.prototype.drawSnake = function () {
    for (let i = 0; i < this.elements.length; i++) {
      this.elements[i].drawCell("blue");
    }
  }

  let snake = new Snake();
  snake.drawSnake();
  //console.dir(snake.elements);


  // Движение змеи
  Snake.prototype.route = function () {
    let head = this.elements[0];
    let newHead;
    this.way = this.nextWay;
    // Присваиваем направление 
    if (this.way === "right") {
      newHead = new Cell(head.col + 1, head.row);
    } else if (this.way === "left") {
      newHead = new Cell(head.col - 1, head.row)
    } else if (this.way === "up") {
      newHead = new Cell(head.col, head.row - 1)
    } else if (this.way === "down") {
      newHead = new Cell(head.col, head.row + 1)
    }
    if (this.clash(newHead)) {
      stopGame();
    }
    this.elements.unshift(newHead);

    if (newHead.eat(rabbit.point)) {
      score++;
      rabbit.run();
    } else {
      this.elements.pop();
    }
  };

  //Обработка столкновений

  Snake.prototype.clash = function (head) {
    //Border clash
    const rightClash = (head.col === boardWidth - 1);
    const leftClash = (head.col === 0);
    const topClash = (head.row === 0);
    const bottomClash = (head.row === boardHeight - 1);

    const borderClash = rightClash || leftClash || topClash || bottomClash;

    //Столкновение с самим собой

    let snakeClash = false;
    for (let i = 0; i < this.elements.length; i++) {
      if (head.eat(this.elements[i])) {
        snakeClash = true;
      }
    }
    return borderClash || snakeClash;
  }

  //Обработка нажатий
  const ways = {
    39: "right",
    37: "left",
    38: "up",
    40: "down"
  };
  document.body.addEventListener("keydown", (event) => {
    let newWay = ways[event.keyCode];
    if (newWay !== undefined) {
      snake.setWay(newWay);


    }
  });

  //Запрет движения в обратную сторону
  Snake.prototype.setWay = function (newWay) {
    if (this.way === "up" && newWay === "down") {
      return;
    } else if (this.way === "right" && newWay === "left") {
      return;
    } else if (this.way === "down" && newWay === "up") {
      return;
    } else if (this.way === "left" && newWay === "right") {
      return;
    }
    this.nextWay = newWay;
  };

  class Rabbit {
    constructor() {
      this.point = new Cell(10, 10);
    }
    drawRabbit(color) {
      this.point.drawCell(color);
    }
  }

  //перемещаем кролика
  Rabbit.prototype.run = function () {
    const colRandom = Math.floor(Math.random() * (boardWidth - 2)) + 1;
    const rowRandom = Math.floor(Math.random() * (boardHeight - 2)) + 1;
    this.point = new Cell(colRandom, rowRandom);
  };


  let cols = [];
  let rows = [];
  let rabbit = new Rabbit();
  rabbit.run();

  //запуск функции
  let gameLoop = setInterval(function () {
    cols.length = 0;
    rows.length = 0;
    ctx.clearRect(0, 0, width, height);
    counter();
    snake.drawSnake();
    snake.route();
    rabbit.drawRabbit("DeepPink");
    border(ctx, "Green");
    for (let i = 0; i < snake.elements.length; i++) {
      cols.push(snake.elements[i].col)
      rows.push(snake.elements[i].row)
    }
  }, 100);

};