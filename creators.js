export function divCreator(obj, className) {
  const createDiv = document.createElement("div");
  createDiv.className = className;
  return obj.appendChild(createDiv);
}

export function buttonCreator(obj, innerText, className, id) {
  const createButton = document.createElement("button");
  createButton.innerText = innerText;
  createButton.className = className;
  createButton.setAttribute("id", id);
  return obj.appendChild(createButton);
}

export function buttonOfDiv(obj, innerHTML, className, id) {
  const divButton = document.createElement("div");
  divButton.innerHTML = innerHTML;
  divButton.className = className;
  divButton.setAttribute("id", id);
  return obj.appendChild(divButton);
}
export function formCreator(obj, className, innerHTML, actionText, id) {
  const createForm = document.createElement("form");
  createForm.className = className;
  createForm.innerHTML = innerHTML;
  createForm.setAttribute("action", actionText);
  createForm.setAttribute("id", id);
  return obj.appendChild(createForm);
}

export function inputCreator(obj, type, className, placeholder, id) {
  const createInput = document.createElement("input");
  createInput.setAttribute("type", type);
  createInput.className = className;
  createInput.setAttribute("placeholder", placeholder);
  createInput.setAttribute("id", id);
  //createInput.insertAdjacentHTML("beforebegin", beforebegin);
  return obj.appendChild(createInput);
}


