const loginItem = localStorage.getItem("userInLogin");

//Обработка формы регистрации

// Создание логина
export function newUserLogin() {
  const userGetName = document.querySelector("#regName").value;
  const userRegName = userGetName.trim();
  if (userRegName.length < 4 || userRegName.length > 20) {
    document.querySelector("#regName").value = "";
    alert("Имя должно быть 4 - 20 символов");
    return;
  }
  for (let i = 0; i < localStorage.length; i++) {
    const userKey = localStorage.key(i);
    if (userRegName === userKey) {
      alert("Логин уже занят");
      document.querySelector("#regName").value = "";
      return;
    }
  }
  return userRegName;
};


// Создание пароля
export function newUserPassword() {
  const userRegPass = document.querySelector("#regPass").value;
  const userRegRepass = document.querySelector("#regRepass").value;
  if (userRegPass.length < 6 || userRegPass.length > 20) {
    alert("Длина пароля от 6 до 20 символов");
    document.querySelector("#regPass").value = "";
    return;
  }
  if (userRegRepass.length < 6 || userRegRepass.length > 20) {
    alert("Длина пароля от 6 до 20 символов");
    document.querySelector("#regPass").value = "";
    document.querySelector("#regRepass").value = "";
    return;
  } else if (userRegPass != userRegRepass) {
    document.querySelector("#regPass").value = "";
    document.querySelector("#regRepass").value = "";
    alert("Пароли не совпадают");
    return;
  }
  return userRegPass;
};

// Pегистрация нового пользователя
export function newUserRegistration() {
  const newLogin = newUserLogin();
  const newPass = newUserPassword();
  const regForm = document.getElementById("regForm");
  if (newLogin == undefined || newPass == undefined) {
    console.log("Проверьте введённые данные")
    return;
  }
  const user = {
    name: newLogin,
    password: newPass,
    score: ""
  }

  localStorage.setItem(newLogin, JSON.stringify(user));

  alert("Вы успешно зарегестрированы");
  regForm.classList.toggle("active");
  setTimeout(function () {
    window.location.reload();
    }, 100);
};


//Обработка формы Вход
// Получаем логин
export function userEnterName() {
  const userGetName = document.querySelector("#entName").value;
  const userEnterName = userGetName.trim();
  return userEnterName;
}
// Получаем пароль
export function userEnterPass() {
  const userEnterPass = document.querySelector("#entPass").value;
  return userEnterPass;
};

// Проверяем логин пароль и логинимся
export function userEnter() {
  const userName = userEnterName();
  const userPass = userEnterPass();
  if (userName == "") {
    alert("Вы не ввели имя");
    return;
  }
  if (userPass == "") {
    alert("Вы не ввели пароль");
    return;
  }

  for (let i = 0; i < localStorage.length; i++) {
    const userKey = localStorage.key(i);
    if (userName === userKey) {
      const generalName = userName;
      const exitButton = document.querySelector(".exitButton")
      const getUserObject = localStorage.getItem(generalName);
      const parsePass = JSON.parse(getUserObject);
      const getPass = parsePass.password;
      if (userPass !== getPass) {
        alert("неверное имя или пароль");
        document.querySelector("#entName").value = "";
        document.querySelector("#entPass").value = "";
      }
      if (userPass === getPass) {
        setUserInLogin(generalName);
        alert("Добро пожаловать " + generalName + " !");
        exitButton.classList.toggle("active");
        setTimeout(function () {
          window.location.reload();
          }, 100);
      }
    }
  }
};

//Отмечаем залогиненого пользователя
export function setUserInLogin(user) {
  localStorage.setItem("userInLogin", JSON.stringify(user));
};

//Отображаем какой пользователь сейчас залогинен
export function userInLoginKey() {
  const userInLoginKey = localStorage.getItem("userInLogin");
  const getUserInLoginKey = JSON.parse(userInLoginKey);

  return getUserInLoginKey;
};

//Выход из логина
export function loginEsc() {
  localStorage.removeItem("userInLogin");
  enterForm.classList.toggle("active");
  setTimeout(function () {
    window.location.reload();
    }, 100);
};

//Переключатель окон
export function formToggle() {
  regForm.classList.toggle("active");
  enterForm.classList.toggle("active");
};

export function gameButtonToggle() {
  gameButton.classList.toggle("active");
}

export function repeatButtonToggle() {
  repeatButton.classList.toggle("active");

}

export function escButtonToggle() {
  escGameButton.classList.toggle("active");
}

export function escGame() {
  document.querySelector(".canWrapper").remove();
  escButtonToggle();
  repeatButtonToggle();
  gameButtonToggle();



}
//Проверка есть ли залогиненый пользователь
export function loginCheck() {
  if (loginItem) {
    const header = document.querySelector(".header");
    const gameButton = document.querySelector(".gameButton");
    header.classList.toggle("active");
    gameButton.classList.toggle("active");
  } else {
    const enterForm = document.querySelector(".enterForm");
    enterForm.classList.toggle("active");
  }
};

